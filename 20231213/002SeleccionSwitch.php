<?php

// Crear una variable llamada color que puede valer 
// R
// V
// A
// Imprimir los siguientes valores en función del color
// R ==> rojo
// V ==> verde
// A ==> Azul
// Realizarlo con switch

$color = 'r';

switch ($color) {
    case 'R':
    case 'r':
        echo 'rojo';
        break;
    case 'V':
    case 'v':
        echo 'verde';
        break;
    case 'A':
    case 'a':
        echo 'azul';
        break;
    default:
        echo 'color no valido';
}
