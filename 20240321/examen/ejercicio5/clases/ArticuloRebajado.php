<?php

namespace clases;


final class ArticuloRebajado extends Articulo
{
    private float $rebaja;

    public function __construct(string $pNombre, float $pPrecio, float $rebaja)
    {
        parent::__construct($pNombre, $pPrecio);
        $this->rebaja = $rebaja;
    }

    private function calculaDescuento()
    {
        return $this->precio * $this->rebaja / 100;
    }

    public function precioRebajado()
    {
        return $this->precio - $this->calculaDescuento();
    }

    public function __toString()
    {
        $articulo = parent::__toString();
        $fichero = file_get_contents("clases/plantillas/articuloRebajado.tpl");
        $articuloRebajado = str_replace(
            ["%rebaja%", "%descuento%", "%final%"],
            [$this->rebaja, $this->calculaDescuento(), $this->precioRebajado()],
            $fichero
        );
        return $articulo . $articuloRebajado;
    }
}
