<?php

namespace clases;

class Articulo
{
    protected ?string $nombre = null;
    protected float $precio = 0;

    public function __construct(?string $nombre = null, float $precio = 0)
    {
        $this->nombre = $nombre;
        $this->precio = $precio;
    }

    public function __toString()
    {
        $resultado = file_get_contents('clases/plantillas/articulo.tpl');
        return str_replace(
            ["%nombre%", "%precio%"],
            [$this->nombre, $this->precio],
            $resultado
        );
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function getPrecio(): float
    {
        return $this->precio;
    }

    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    public function setPrecio(float $precio)
    {
        $this->precio = $precio;
        return $this;
    }
}
