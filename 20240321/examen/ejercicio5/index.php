<?php

use clases\Articulo;

// autocarga de clases
spl_autoload_register(function ($clase) {
    require $clase . '.php';
});

$articulo1 = new Articulo("tornillos");

echo $articulo1;

$articulo1->setPrecio(10);
echo $articulo1;

// $articulo1->precio=10; produce error

$articulo2 = new \clases\ArticuloRebajado("toallas", 25, 10);

echo $articulo2;
