<?php
if (!$_POST) {
    header("Location: 1paso.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1 paso</title>
    <link rel="stylesheet" href="../css/main.css">
</head>

<body>
    <div class="container">
        <div class="row mt-5">
            <div class="color1 rounded text-white p-5">
                <h1>Ejercicio Numero 1 del examen de php</h1>
                <div class="lead">Escribe una palabra en cada caja de texto y te dire si se ha repetido alguna</div>
            </div>
        </div>

        <form class="row mt-5 bg-light p-5" method="POST" action="3paso.php">
            <div class="mb-3">
                <h2 class="text-color1">
                    Paso 2
                </h2>
            </div>
            <?php
            for ($c = 0; $c < $_POST['numero']; $c++) {
                include '_caja.php';
            }
            ?>
            <div class="mb-3">
                <button type="submit" class="btn btn-primary">Comprobar</button>
                <button type="reset" class="btn btn-danger">Borrar</button>
            </div>
        </form>

        <div class="row mt-5 mb-0">
            <div class="text-light p-5 alert color1">
                Ejercicio del examen de PHP - Ramon Abramo
            </div>
        </div>

        <div class="row mt-2">
            <img src="../imgs/logo.png" class="col-lg-2 col-sm-4 d-block mx-auto">
        </div>


    </div>

</body>

</html>