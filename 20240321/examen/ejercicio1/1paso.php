<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1 paso</title>
    <link rel="stylesheet" href="../css/main.css">
</head>

<body>
    <div class="container">
        <div class="row mt-5">
            <div class="color1 rounded text-white p-5">
                <h1>Ejercicio Numero 1 del examen de php</h1>
                <div class="lead">Escriba un numero entre 0 y 10 y dibujare una tabla de una columna de ese tamaño con cajas de texto en cada celda</div>
            </div>
        </div>

        <form class="row mt-5 bg-light p-5" method="POST" action="2paso.php">
            <div class="mb-3">
                <h2 class="text-color1">
                    Paso 1
                </h2>
            </div>
            <div class="input-group mb-3">
                <span class="input-group-text">Tamaño de la tabla</span>
                <input class="form-control" type="number" name="numero" required>
            </div>
            <div class="mb-3">
                <button type="submit" class="btn btn-primary">Dibujar</button>
                <button type="reset" class="btn btn-danger">Borrar</button>
            </div>
        </form>

        <div class="row mt-5 mb-0">
            <div class="text-light p-5 alert color1">
                Ejercicio del examen de PHP - Ramon Abramo
            </div>
        </div>

        <div class="row mt-2">
            <img src="../imgs/logo.png" class="col-lg-2 col-sm-4 d-block mx-auto">
        </div>


    </div>

</body>

</html>