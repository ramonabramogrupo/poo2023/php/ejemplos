<?php
if ($_POST) {
    // leemos los valores escritos en el paso 2
    $valores = $_POST['texto'];
} else {
    // si no hay POST, redireccionamos a 1paso.php
    header("Location: 1paso.php");
}

/**
 * Metodo 1
 */

function calculos(array $valores): string
{
    $mensaje = "";
    // creamos un array asociativo con las repeticiones de cada valor en el array pasado
    $repeticiones = array_count_values($valores);

    // contamos cuantos elementos tenemos en el array pasado
    $total = count($valores);

    // 
    if (array_key_exists(null, $repeticiones)) {
        $rellenadas = $total - $repeticiones[null];
        $mensaje = "<div>Hay $rellenadas elementos rellenos de un total de $total</div>";
    } else {
        $mensaje = "<div> Has rellenado los $total elementos</div>";
    }

    if (count($repeticiones) == $total) {
        $mensaje .= "<div>No hay elementos repetidos</div>";
    } else {
        $mensaje .= "<div>Hay elementos repetidos</div>";
    }

    return $mensaje;
}

function calculos1(array $valores): string
{
    $mensaje = "";

    // cuento cuantos valores hay en el array pasado
    $total = count($valores);

    // dejo en el array solo los elementos que estan vacios y los cuento
    $vacios = count(array_filter($valores, function ($valor) {
        return empty($valor);
    }));


    if ($vacios == 0) {
        $mensaje = "<div>Has rellenado todos los elementos</div>";
    } else {
        $mensaje = "<div>Hay " . ($total - $vacios) . " elementos rellenos de un total de {$total}</div>";
    }


    // para saber si tengo elementos repetidos

    // creo un array sin elementos repetidos
    $sinRepetidos = array_unique($valores);

    if (count($sinRepetidos) == $total) {
        $mensaje .= "<div>No hay elementos repetidos</div>";
    } else {
        $mensaje .= "<div>Hay elementos repetidos</div>";
    }

    return $mensaje;
}

function calculos2(array $valores): string
{
    $mensaje = "";

    // cuento cuantos valores hay en el array pasado
    $total = count($valores);

    // voy a  recorrer el array y compruebo cuantos vacios
    foreach ($valores as $valor) {
        $vacios = 0;
        if ($valor == null) {
            $vacios++;
        }
    }

    if ($vacios == 0) {
        $mensaje = "<div>Has rellenado todos los elementos</div>";
    } else {
        $mensaje = "<div>Hay " . ($total - $vacios) . " elementos rellenos de un total de {$total}</div>";
    }

    // compruebo si hay repetidos
    if ($total == count(array_unique($valores))) {
        $mensaje .= "<div>No hay elementos repetidos</div>";
    } else {
        $mensaje .= "<div>Hay elementos repetidos</div>";
    }
    return $mensaje;
}

// $mensaje = calculos($valores);
// $mensaje = calculos1($valores);
$mensaje = calculos2($valores);


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>1 paso</title>
    <link rel="stylesheet" href="../css/main.css">
</head>

<body>
    <div class="container">
        <div class="row mt-5">
            <div class="color1 rounded text-white p-5">
                <h1>Ejercicio Numero 1 del examen de php</h1>
                <div class="lead">Paso 3</div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="card col-lg-4 color1 text-light">
                <div class="card-body">
                    <h5 class="card-title">Resultados</h5>
                    <p class="card-text">
                        <?php
                        echo $mensaje;
                        ?>
                    </p>
                </div>
            </div>
            <div class="card col-lg-4 offset-lg-2">
                <div class="card-body">
                    <h5 class="card-title">Valores</h5>
                    <div class="card-text">
                        <ul class="list-group">
                            <?php
                            foreach ($valores as $valor) {
                                echo "<li class='list-group-item'>" . ($valor ?: "vacio") . "</li>";
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <div class="row mt-5 mb-0">
            <div class="text-light p-5 alert color1">
                Ejercicio del examen de PHP - Ramon Abramo
            </div>
        </div>

        <div class="row mt-2">
            <img src="../imgs/logo.png" class="col-lg-2 col-sm-4 d-block mx-auto">
        </div>


    </div>

</body>

</html>