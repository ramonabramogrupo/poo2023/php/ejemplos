<?php

namespace clases;

class Imagen
{
    private string $src;
    private bool $border = false;
    private ?int $ancho = null;
    private ?int $alto = null;

    const RUTA = 'images/';
    const PLANTILLA = __NAMESPACE__ . "/plantilla.php";

    public function __construct(string $src, bool $border = false, ?int $ancho = null, ?int $alto = null)
    {
        // el src lo asigno a traves del setter
        //$this->src = $src;
        $this->setSrc($src);

        $this->border = $border;
        $this->ancho = $ancho;
        $this->alto = $alto;
    }

    /**
     * 
     * Mostrar el contenido del objeto utilizando buffer de salida
     */

    public function __toStringFlujo(): string
    {
        ob_start(); // almaceno la salida en un buffer
?>
        <img <?= $this->getSrc() . $this->getBorder() . $this->getAncho() . $this->getAlto() ?> ">
    <?php
        return ob_get_clean();
    }


    public function __toStringVariable(): string
    {
        $resultado = "<img "; // abriendo etiqueta img
        $resultado .= $this->getSrc();
        $resultado .= $this->getBorder();
        $resultado .= $this->getAncho();
        $resultado .= $this->getAlto();
        $resultado .= ">"; // cerrando etiqueta img

        return $resultado;
    }

    public function __toString()
    {
        $resultado = file_get_contents(self::PLANTILLA);
        return str_replace(
            [
                "{{src}}",
                "{{border}}",
                "{{width}}",
                "{{height}}"
            ],
            [
                $this->getSrc(),
                $this->getBorder(),
                $this->getAncho(),
                $this->getAlto()
            ],
            $resultado
        );
    }

    public function getSrc(): string
    {

        return 'src="' . self::RUTA . $this->src . '"';
    }

    public function getBorder(): string
    {
        //return " border=\"$this->border\" ";
        return ' border="' . (int)$this->border . '"';
    }

    public function getAncho(): string
    {
        if (is_null($this->ancho)) {
            return "";
        } else {
            return " width=\"$this->ancho\" ";
        }
    }

    public function getAlto(): string
    {
        if (!isset($this->alto)) {
            return "";
        } else {
            return " height=\"$this->alto\" ";
        }
    }

    public function setSrc(string $src)
    {
        if (file_exists(self::RUTA . $src)) {
            $this->src = $src;
        } else {
            die("no existe foto");
        }

        return $this; // esto es lo que hace que el setter sea fluent
    }

    public function setBorder(bool $border)
    {
        $this->border = $border;
        return $this; // esto es lo que hace que el setter sea fluent
    }

    public function setAncho(?int $ancho)
    {
        $this->ancho = $ancho;
        return $this; // esto es lo que hace que el setter sea fluent
    }

    public function setAlto(?int $alto)
    {
        $this->alto = $alto;
        return $this; // esto es lo que hace que el setter sea fluent
    }
}
