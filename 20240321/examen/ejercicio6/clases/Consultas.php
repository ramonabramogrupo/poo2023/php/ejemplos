<?php

namespace clases;

use mysqli;
use mysqli_result;

class Consultas
{
    private mysqli $db;
    private mysqli_result $resultados;
    public array $datos = [];
    private array $campos = [];
    public string $tabla = "";

    private string $pk = 'id';

    public function __construct(mysqli $db, string $tabla = "")
    {
        $this->db = $db;
        if ($tabla != "") {
            $this->setTabla($tabla);
        }
    }

    public function query($consulta): mysqli_result
    {
        $this->resultados = $this->db->query($consulta);
        return $this->resultados;
    }

    public function findAll(): self
    {
        $consulta = "SELECT * FROM {$this->tabla}";
        $this->query($consulta);
        return $this;
    }

    public function ejecutar()
    {
        $registros = $this->resultados->fetch_all(MYSQLI_ASSOC);
        $this->resultados->data_seek(0);
        return $registros;
    }

    public function findOne($pk)
    {
        $consulta = "SELECT * FROM {$this->tabla} WHERE {$this->pk} = '{$pk}'";
        $this->query($consulta);
        return $this->resultados;
    }


    public function delete(int $id)
    {
        $consulta = "DELETE FROM {$this->tabla} WHERE {$this->pk} = '{$id}'";
        $this->query($consulta);
    }
    public function insertar()
    {
        // para conseguir esto
        //$this->campos = ["titulo", "paginas", "fechaPublicacion"];
        //$campos = "titulo,paginas,fechaPublicacion";

        // lo realizo asi
        $campos = implode(",", $this->campos);

        // para conseguir esto
        // $this->datos = [
        //     "paginas" => "10",
        //     "fechaPublicacion" => "2022-10-10",
        //     "titulo" => "hola",
        // ];
        // $datos="'hola','10','2022-10-10'"

        //para conseguirlo
        foreach ($this->campos as $campo) {
            $valores[] = "'{$this->datos[$campo]}'";
        }
        $datos = implode(',', $valores);
        $consulta = "INSERT INTO {$this->tabla}({$campos}) VALUES ({$datos})";

        $this->db->query($consulta);
    }

    public function setDatos(array $parametros = []): self
    {
        if (count($parametros) == 0) {
            foreach ($this->campos as $campo) {
                $this->datos[$campo] = "";;
            }
        } else {
            // asigno los datos a un array asociativo
            foreach ($this->campos as $campo) {
                $this->datos[$campo] = $parametros[$campo];
            }
        }
        return $this;
    }

    public function setCampos(array $campos): self
    {
        // inicializo el array de campos
        $this->campos = $campos;

        // inicializo los datos
        foreach ($this->campos as $campo) {
            $this->datos[$campo] = "";;
        }
        return $this;
    }


    public function listar(array $botones = [], array $campos = []): string
    {
        if ($this->resultados->num_rows > 0) {
            $registros = $this->resultados->fetch_all(MYSQLI_ASSOC);
            // mostrar los registros
            $salida = "<table class='table table-striped table-bordered'>";
            $salida .= "<thead class='table-dark'><tr>";

            // compruebo si el usuario me ha pasado los campos a mostrar
            if (count($campos) == 0) {
                // si no me ha pasado los campos los leo del primer registro de la base de datos
                $campos = array_keys($registros[0]);
            }

            foreach ($campos as $campo) {
                $salida .= "<th>$campo</th>";
            }
            if (count($botones) > 0) {
                // añado una columna para los botones
                $salida .= "<td>Acciones</td>";
            }

            $salida .= "</tr></thead>";
            // muestro todos los registros
            foreach ($registros as $registro) {
                $salida .= "<tr>";
                // mostrando los campos que hayamos seleccionado
                foreach ($campos as $campo) {
                    $salida .= "<td>" . $registro[$campo] . "</td>";
                }
                // mostrando los botones
                if (count($botones) > 0) {
                    $salida .= "<td>";

                    foreach ($botones as $label => $enlace) {
                        $salida .= "<a href='{$enlace}?id={$registro['id']}'>{$label}</a> ";
                    }

                    // cerramos la celda
                    $salida .= "</td>";
                }

                $salida .= "</tr>";
            }
            $salida .= "</table>";
        } else {
            $salida = "No hay registros";
        }
        // colocar el puntero al principio de los registros
        $this->resultados->data_seek(0);

        return $salida;
    }

    public function setPk(string $pk): self
    {
        $this->pk = $pk;
        return $this;
    }

    public function setTabla(string $tabla): self
    {
        $this->tabla = $tabla;
        $this->obtenerPk(); // obtengo la clave principal
        $this->obtenerCampos(); // obtengo todos los campos sin la clave
        return $this;
    }

    private function obtenerCampos(): void
    {
        $consulta = "SHOW COLUMNS FROM {$this->tabla}";
        $this->query($consulta);
        $campos = $this->resultados->fetch_all(MYSQLI_ASSOC);
        foreach ($campos as $campo) {
            $this->campos[] = $campo['Field'];
        }
        // quitamos la clave principal
        $this->campos = array_diff($this->campos, [$this->pk]);

        // inicializo los datos
        foreach ($this->campos as $campo) {
            $this->datos[$campo] = "";;
        }
    }

    private function obtenerPk(): void
    {
        $consulta = "SHOW KEYS FROM {$this->tabla} WHERE Key_name = 'PRIMARY'";
        $this->query($consulta);
        $this->pk = $this->resultados->fetch_assoc()['Column_name'];
    }
}
