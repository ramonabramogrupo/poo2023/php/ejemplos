<?php

namespace clases;

class Autor extends Modelo
{
    public $nombre;
    public $apellidos;
    public $id;
    protected function campos()
    {
        return "nombre,apellidos";
    }
    protected function primary()
    {
        return "id";
    }
}
