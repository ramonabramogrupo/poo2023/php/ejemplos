<?php


// autocarga de clases

use clases\Consultas;

spl_autoload_register(function ($clase) {
    require $clase . '.php';
});

// creo una conexion
$conexion = new mysqli('localhost', 'root', '', 'pruebaPhp');

// creo una consulta
$consulta = new Consultas($conexion, "autores");

// preparo un nuevo registro
$consulta->setDatos([
    "id" => 1,
    "nombre" => "pedro",
    "apellidos" => "perez"
]);

// inserto el registro
$consulta->insertar();

// pruebo a listar
$salida = $consulta->findAll()->listar();





?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?= $salida ?>
</body>

</html>