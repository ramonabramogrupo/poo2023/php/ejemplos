drop DATABASE if exists pruebaPhp;
CREATE DATABASE pruebaPhp;
use pruebaPhp;

create Table autores(
    id int(11) not null auto_increment,
    nombre varchar(100) not null,
    apellidos varchar(100) not null,
    primary key(id)
);

insert into autores values (1, 'Javier', 'García');
    