<?php

// autocarga de clases

use clases\Imagen;

spl_autoload_register(function ($clase) {
    require $clase . '.php';
});


$foto = new Imagen("1.jpg", false, 100);
echo $foto;
$foto = new Imagen("2.jpg", false, 100);
echo $foto;
$foto = new Imagen("3.jpg", false, 100);
echo $foto;
