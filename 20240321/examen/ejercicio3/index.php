<?php

// autocarga de clases
spl_autoload_register(function ($clase) {
    require $clase . '.php';
});

$foto = new \clases\Imagen("3.jpg");
// para realizar eso
//$foto->setAncho(400);
//$foto->setBorder(true);

// si tenemos el setter fluent
$foto->setAncho(400)->setBorder(true);


echo $foto;
