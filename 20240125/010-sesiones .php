<?php

// 1 paso
// cuando le de al boton jugar me cree tres frutas aletorias
// 2 paso
// cuando le de al boton incrementar credito me sume 1 al credito
// 3 paso
// cuando le de el boton de jugar me permita jugar de la siguiente forma
// si sale una ceraza se gana 1 punto
// si salen dos cerazas se gana 4 puntos
// si salen tres cerezas se gana 10 puntos
// si salen dos frutas iguales que no sean cerezas se ganan 2 puntos
// si salen tres frutas iguales que no sean cerezas se ganan 5 puntos

$numeros = [1, 2, 5];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="formularios.css">
    <link rel="stylesheet" href="tragaperras.css">

</head>

<body>
    <form methid="POST">
        <div class="caja-frutas">
            <span><img src="imgs/<?= $numeros[0] ?>.svg"></span>
            <span><img src="imgs/<?= $numeros[1] ?>.svg"></span>
            <span><img src="imgs/<?= $numeros[2] ?>.svg"></span>
        </div>
        <br>
        <div>
            <label for="creditos">Credito</label>
            <input type="number" name="creditos" id="creditos" readonly>
        </div>
        <br>
        <div>
            <label for="ganancia">Ganancia</label>
            <input type="number" name="ganancias" value="0" readonly>
        </div>
        <br>
        <div>
            <button name="jugar">jugar</button>
            <button name="meter">Incrementar creditos</button>
        </div>

    </form>
</body>

</html>