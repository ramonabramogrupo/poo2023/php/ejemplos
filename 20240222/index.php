<?php
// arranco sesiones
session_start();

// cargo las funciones
require_once "funciones.php";

// control de errores
controlErrores();

// inicializo las variables
$salida = "";
$parametros = require_once "parametros.php";

// compruebo si he pulsado el boton de login
if ($_POST) {
    // creo una variable de sesion para comprobar que estoy logueado
    $_SESSION['nombre'] = $_POST['nombre'];
}

// compruebo si estoy logueado
if (isset($_SESSION['nombre'])) {
    $menu = menu([
        "Inicio" => "index.php",
        "Mensaje" => "mensaje.php",
        "Salir" => "salir.php",
    ]);

    $salida = $menu;
    $salida .= "Bienvenido {$_SESSION['nombre']}";
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

</head>

<body>

    <h1>Inicio</h1>

    <?php
    if (!isset($_SESSION['nombre'])) {
        require "_login.php";
    }
    ?>

    <?= $salida ?>

</body>

</html>