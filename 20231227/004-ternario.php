<?php

$datos = [
    "nombre" => "Pepe",
    "apellidos" => null,
    "edad" => NULL

];

// $nombre = "Pepe";
// $apellidos = "no conocidos";
// $edad = "no conocida";
// quiero realirlo  con el ternario recortado

$nombre = (isset($datos["nombre"])) ? $datos["nombre"] : "no conocido";
$nombre = $datos["nombre"] ?? "no conocido";
$apellidos = $datos["apellidos"] ?? "no conocidos";
$edad = $datos["edad"] ?? "no conocida";

var_dump($nombre, $apellidos, $edad);

// quiero realizarlo con el operador ternario recortado elvis
$nombre = $datos["nombre"] ?: "no conocido";
$apellidos = $datos["apellidos"] ?: "no conocidos";
$edad = $datos["edad"] ?: "no conocida";
var_dump($nombre, $apellidos, $edad);
