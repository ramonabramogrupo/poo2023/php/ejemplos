<?php

$dato = 10;
$valor = null;

?>
<h2> comprobar si existe y es distinta de null la variable</h2>
<?php
// la funcion isset comprueba si una variable existe y es distinta de null
// en caso de que no existe o que sea null devuelve falso
// si la variable vale false tambien devuelve false
//isset($dato); ==> true
//isset($dato1); ==> false
//isset($valor); ==> false
//isset(false); ==> false

// La funcion empty comprueba si una variable es distinta de null
// en caso de que la variable no exista o sea null devuelve true
//empty($dato); ==> false
//empty($dato1); ==> true  
//empty($valor); ==> true
//empty(false); ==> true 



// operador ternario completo
echo isset($dato) ? $dato : 'No hay dato';
// operador ternario recortado para comprobar si existe
echo $dato ?? 'No hay dato';
echo "<br>";

// operador ternario completo
echo isset($dato1) ? $dato1 : 'No hay dato';
// operador ternario recortado para comprobar si existe
echo $dato1 ?? 'No hay dato';
echo "<br>";

// operador ternario completo
echo isset($valor) ? $valor : 'No hay dato';

// operador ternario recortado para comprobar si existe
echo $valor ?? 'No hay dato';
echo "<br>";
