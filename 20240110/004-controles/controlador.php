<?php
// inicializo las variables en el controlador
$numero1 = 0;
$numero2 = 0;
$numeros = "";
$suma = 0;
$numerosMostrar = "";

// he pulsado el boton de enviar
if ($_POST) {
    // almaceno los numeros introducidos
    $numero1 = $_POST["numero1"] ?: 0;
    $numero2 = $_POST["numero2"] ?: 0;
    // un string con los numeros de la tercera caja
    $numeros = $_POST["numeros"] ?: "0";
    // un array con los numeros de la tercera caja
    $numerosArray = explode(";", $_POST["numeros"]);

    // calculo la suma
    $suma = $numero1 + $numero2 + array_sum($numerosArray);

    // convierto el array en un string con un br entre cada numero
    $numeros = implode("<br>", $numerosArray);
    $numerosMostrar = "{$numero1}<br>{$numero2}<br>{$numeros}";
}
