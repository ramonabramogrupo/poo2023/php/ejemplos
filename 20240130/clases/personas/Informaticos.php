<?php
// espacio de nombres
namespace clases\personas;

// definicion de la clase
class Informaticos extends Tecnicos
{
    public string $aula;
    public array $ordenadores;

    public function __construct()
    {
        $this->aula = "";
        $this->ordenadores = [];
    }
}
