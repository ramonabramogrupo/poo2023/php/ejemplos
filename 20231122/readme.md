![php](../imgsReadme/php.png)
<table>
<tr>
<td>

![php](../imgsReadme/lateral1.jpg)

</td>
<td>

- ejemplos
  - eeee


# Dia 1

>✨
>
>En esta clase vamos a introducirnos en php

Para ver una descripcion de los ejemplos [pincha aqui](#todos-los-ejemplos)

## Indice

Para volver al indice general [pincha aqui](../readme.md)

# Objetivo del dia

En clase instalaremos a traves de laragon un servidor local con apache, mysql y el procesador de php integrado.boton

Ademas os explicare en clase como cambiar las versiones de mysql y php en laragon

# Teoria y Diapositivas

En el repositorio no esta la teoria de la clase ni las diapositivas.

De todas formas os voy a ir colocando conceptos de teoria vistos en clase junto con los ejemplos

</td>

</tr>
</table>

# Todos los ejemplos

| Carpeta | Descripcion |
|---|---|
|[20231122](20231122/)|Introduccion a php y html|
|[20240219](20240219/)|Primera aplicacion CRUD utilizando mysqli|