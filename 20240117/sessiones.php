<?php
session_start();

// la variable de sesion persiste mientras mantengas el 
// navegador abierto
if (isset($_SESSION['a'])) {
    $_SESSION['a']++;
} else {
    $_SESSION['a'] = 0;
}

echo $_SESSION['a']; //imprime 0,1,2,3,4,5,6,7,8,9


// las variables solo pueden usarse en esta pagina
// cuando la pagina termina de cargarse se eliminan
if (isset($a)) {
    $a++;
} else {
    $a = 0; // 0
}

echo $a; // imprime 0 siempre
