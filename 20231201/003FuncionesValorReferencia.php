<?php

function crear($numero1, &$numero2)
{
    $a = 10;
    $b = 5;
    $numero1 = 100;
    $numero2 = 43;
    //  no puedo acceder a notas porque no es local
    // a la funcion
}


// creo unas variables fuera de la funcion
$a = $b = $numero1 = $numero2 = 0;
$notas = [1, 2];

// llamo a la funcion
crear($numero1, $numero2);

// indica que valor tendra cada variable
// a : 0
// b : 0
// numero1 : 0
// numero2 : 43

// con un var_dump mostremos los valores de las variables
var_dump($a, $b, $numero1, $numero2);


// llamo a la funcion 
crear($a, $b);

// indica que valor tendra cada variable
// a : 0
// b :  43
// numero1 : 0
// numero2 : 43

// con un var_dump mostremos los valores de las variables
var_dump($a, $b, $numero1, $numero2);

// llamo a la funcion
crear($a, $notas);

// indica que valor tendra cada variable
// a : 0
// b :  43
// numero1 : 0
// numero2 : 43
// notas : 43

var_dump($a, $b, $numero1, $numero2, $notas);
