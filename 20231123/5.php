<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        td {
            border: 1px solid black;
            background-color: gray;
            width: 200px;
        }
    </style>

</head>

<body>
    <?php
    $nombre = "Ramon";
    $edad = 51;
    $poblacion = "Santander";
    ?>
    <table>
        <tr>
            <td>Nombre</td>
            <td><?= $nombre ?></td>
        </tr>
        <tr>
            <td>Edad</td>
            <td><?= $edad ?></td>
        </tr>
        <tr>
            <td>Poblacion</td>
            <td><?= $poblacion ?></td>
        </tr>
    </table>
</body>

</html>