<?php

namespace clases;

class Dimension
{
    private float $alto;
    private float $ancho;
    private float $profundidad;
    public function __construct()
    {
        $this->alto = 0;
        $this->ancho = 0;
        $this->profundidad = 0;
    }

    /**
     * Get the value of alto
     *
     * @return float
     */
    public function getAlto(): float
    {
        return $this->alto;
    }

    /**
     * Set the value of alto
     *
     * @param float $alto
     *
     * @return self
     */
    public function setAlto(float $alto): self
    {
        $this->alto = $alto;

        return $this;
    }

    /**
     * Get the value of ancho
     *
     * @return float
     */
    public function getAncho(): float
    {
        return $this->ancho;
    }

    /**
     * Set the value of ancho
     *
     * @param float $ancho
     *
     * @return self
     */
    public function setAncho(float $ancho): self
    {
        $this->ancho = $ancho;

        return $this;
    }

    /**
     * Get the value of profundidad
     *
     * @return float
     */
    public function getProfundidad(): float
    {
        return $this->profundidad;
    }

    /**
     * Set the value of profundidad
     *
     * @param float $profundidad
     *
     * @return self
     */
    public function setProfundidad(float $profundidad): self
    {
        $this->profundidad = $profundidad;

        return $this;
    }

    public function getVolumen(): float
    {
        return $this->alto * $this->ancho * $this->profundidad;
    }

    public function __toString(): string
    {
        $salida = "Alto: " . $this->alto . " ,Ancho: " . $this->ancho . " ,Profundidad: " . $this->profundidad . " ,Volumen: " . $this->getVolumen();
        return $salida;
    }
}
