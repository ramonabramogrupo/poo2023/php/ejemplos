<?php

namespace clases;

class Planeta
{
    public ?string $nombre;
    public int $cantidadSatelites;
    public float $masa;
    public float $volumen;
    public int $diametro;
    public int $distanciaAlSol;
    private string $tipo;
    public bool $observableASimpleVista;

    // constructor
    // opcion 1
    // public function __construct()
    // {
    //     $this->nombre = null;
    //     $this->cantidadSatelites = 0;
    //     $this->masa = 0;
    //     $this->volumen = 0;
    //     $this->diametro = 0;
    //     $this->distanciaAlSol = 0;
    //     $this->tipo = "";
    //     $this->observableASimpleVista = false;
    // }

    // constructor
    // opcion 2
    public function __construct(?string $nombre = null, int $cantidadSatelites = 0, float $masa = 0, float $volumen = 0, int $diametro = 0, int $distanciaAlSol = 0, string $tipo = "", bool $observableASimpleVista = false)
    {
        $this->nombre = $nombre;
        $this->cantidadSatelites = $cantidadSatelites;
        $this->masa = $masa;
        $this->volumen = $volumen;
        $this->diametro = $diametro;
        $this->distanciaAlSol = $distanciaAlSol;
        $this->tipo = "";
        $this->setTipo($tipo);
        $this->observableASimpleVista = $observableASimpleVista;
    }

    public function imprimir(): string
    {
        $salida = "<h2>{$this->nombre}</h2>";
        $salida .= "<p>Cantidad de Satelites: {$this->cantidadSatelites}</p>";
        $salida .= "<p>Masa: {$this->masa}</p>";
        $salida .= "<p>Volumen: {$this->volumen}</p>";
        $salida .= "<p>Diametro: {$this->diametro}</p>";
        $salida .= "<p>Distancia al Sol: {$this->distanciaAlSol}</p>";
        $salida .= "<p>Tipo: {$this->tipo}</p>";
        $salida .= "<p>Observable a simple vista:" . $this->getObservable() . "</p>";
        return $salida;
    }

    // añadir un getter para observable
    private function getObservable(): string
    {
        $salida = $this->observableASimpleVista ? "Si" : "No";
        return $salida;
    }

    // calcular la densidad de un planeta
    // como el cocientes entre su masa y su volumen
    public function densidad(): float
    {
        return ($this->masa / $this->volumen);
    }

    // determinar si el planeta es exterior
    public function esExterior(): bool
    {
        $ua = 149597870;
        return ($this->distanciaAlSol > 3.4 * $ua);
    }


    public function setTipo($tipo)
    {
        if (in_array($tipo, ["gaseoso", "terrestre", "enano"])) {
            $this->tipo = $tipo;
        }
    }
}
