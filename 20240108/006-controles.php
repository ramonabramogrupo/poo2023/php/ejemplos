<?php
// inicializar las variables
$nombre = "";
$apellidos = "";
$estado = "";
$aficiones = "";

// controlar que he pulsado el boton de enviar
if ($_POST) {

    // leyendo los datos
    $nombre = $_POST["nombre"] ?: "";
    $apellidos = implode(" ", $_POST["apellidos"]);
    // conotrolo si no he seleccionado nada con un ternario nullable
    $estado = $_POST["estado"] ?? "";
    // controlo si no he seleccionada nada con un ternario
    $aficiones = isset($_POST["aficiones"]) ? implode(",", $_POST["aficiones"]) : "";
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form method="post">
        <div>
            <label for="nombre">Nombre</label>
            <input type="text" id="nombre" name="nombre">
        </div>
        <div>
            <label for="apellido1">Apellido 1</label>
            <input type="text" id="apellido1" name="apellidos[]">
        </div>
        <div>
            <label for="apellido2">Apellido 2</label>
            <input type="text" id="apellido2" name="apellidos[]">
        </div>
        <div>
            <span>Estado civil</span><br>
            <label for="casada">Casado/a</label>
            <input type="radio" id="casada" name="estado" value="casada">
            <label for="soltera">Soltero/a</label>
            <input type="radio" id="soltera" name="estado" value="soltera">
        </div>
        <div>
            <span>Aficiones</span><br>
            <label for="deportes">Deportes</label>
            <input type="checkbox" id="deportes" name="aficiones[]" value="deportes">
            <label for="leer">Lectura</label>
            <input type="checkbox" id="leer" name="aficiones[]" value="leer">
            <label for="dormir">Dormir</label>
            <input type="checkbox" id="dormir" name="aficiones[]" value="dormir">
        </div>
        <div>
            <button>Enviar</button>
            <button type="reset">Borrar</button>
        </div>

    </form>
    <?php
    if ($_POST) {
    ?>
        <div>
            <p>Nombre: <?= $nombre ?></p>
            <p>Apellidos: <?= $apellidos ?></p>
            <p>Estado civil: <?= $estado ?></p>
            <p>Aficiones: <?= $aficiones ?></p>
        </div>
    <?php
    }
    ?>
</body>

</html>