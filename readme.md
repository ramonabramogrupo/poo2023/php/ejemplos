![php](imgsReadme/php.png)

# Estructura completa del repositorio

- [ ] [Pagina Inicio](https://gitlab.com/ramonabramogrupo/poo2023)

- [ ] [Php](https://gitlab.com/ramonabramogrupo/poo2023/php)
  - [X] Ejemplos
  - [ ] Practicas

- [ ] Framework Php
- [ ] Yii
- [ ] Laravel

<table>
<tr>
<td>

![php](imgsReadme/lateral.jpg)

</td>
<td>

# Ejemplos de Programación en PHP para el Curso de POO

>✨
>
>Durante este curso, exploraremos ejemplos prácticos de programación en PHP que ilustran los conceptos fundamentales de la programación orientada a objetos (POO). 

Para ver una descripcion de los ejemplos [pincha aqui](#todos-los-ejemplos)

# Objetivo del repositorio 

Estos ejemplos están disponibles en este repositorio de GitLab, y los utilizaremos para comprender cómo aplicar los principios de la POO en el contexto de PHP.

Ademas los ejemplos tambien tratan conceptos basicos de programacion.

# Teoria y Diapositivas

En el repositorio no esta la teoria de la clase ni las diapositivas.

De todas formas os voy a ir colocando conceptos de teoria vistos en clase junto con los ejemplos

</td>

</tr>
</table>

# Todos los ejemplos

| Carpeta | Descripcion |
|---|---|
|[20231122](20231122/)|Introduccion a php y html|
|[20240219](20240219/)|Primera aplicacion CRUD utilizando mysqli|