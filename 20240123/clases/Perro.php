<?php
class Perro
{
    // Atributos
    public $nombre;

    // Metodos
    public function ladrar()
    {
        echo "GUAU GUAU";
    }

    // metodo constructor
    // es recomendable utilizarle para
    // inicializar el objeto
    public function __construct($nombre = "")
    {
        $this->nombre = $nombre;
    }
}
